builddebug:
	cd lzw-rs && cargo build

buildrealese:
	cd lzw-rs && cargo build --release

cpdebug:
	cp lzw-rs/target/debug/liblzw.so .

cprelease:
	cp lzw-rs/target/release/liblzw.so .

default: cprelease buildrealese
