from . import ffi

def compress_file():
    """
        Compresses `input.txt` to `output.lzw`.
    """
    ffi.compress_file()

def decompress_file():
    """
        Decompresses `input.lzw` to `output.txt`.
    """
    ffi.decompress_file()

def compress_file_named(filename_in: str, filename_out: str):
    """
        Compresses the file located in `filename_in`
        path to another file with `filename_out` path.
    """
    ffi.compress_input_filename(filename_in, filename_out)

def decompress_file_named(filename_in: str, filename_out: str):
    """
        Decompresses the file located in `filename_in`
        path to another file with `filename_out` path.
    """
    ffi.decompress_input_filename(filename_in, filename_out)
