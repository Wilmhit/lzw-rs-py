from cffi import FFI
from os import path

ffi = FFI()

ffi.cdef("""
    void compress_input_file();
    void decompress_input_file();
    void compress_given_file(char*, char*);
    void decompress_given_file(char*, char*);
""")

so_path = path.join(path.abspath(path.dirname(__file__)), "../lzw-rs/target/release/liblzw.so")
lzw_rs = ffi.dlopen(so_path)

def compress_file():
    lzw_rs.compress_input_file()

def decompress_file():
    lzw_rs.decompress_input_file()

def compress_input_filename(filename_in: str, filename_out: str):
    lzw_rs.compress_given_file(filename_in.encode(), filename_out.encode())

def decompress_input_filename(filename_in: str, filename_out: str):
    lzw_rs.decompress_given_file(filename_in.encode(), filename_out.encode())
